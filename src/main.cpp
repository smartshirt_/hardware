#include <Arduino.h>
#include <PulseSensorBPM.h>
#include <CurieIMU.h>
#include <Adafruit_MLX90614.h>
#include <Wire.h>
#include <CurieBLE.h>

/*
****
Bluetooth Low Energy vars & characteristics
****
*/
BLEPeripheral blePeripheral;       // BLE Peripheral Device (the board you're programming)


BLEService heartRateService("180D"); // BLE Heart Rate Service
BLELongCharacteristic heartRateChar("49d1f43c-8f1c-4a78-a83f-cdb81da0052d",  BLERead | BLENotify);


BLEService stepCountService("1814");
BLELongCharacteristic stepCountChar("a676dc00-882b-406e-bbce-7feb6978dcac",  BLERead | BLENotify);


BLEService temperatureService("1809");
BLEFloatCharacteristic temperatureChar("04f46f5e-252e-4974-b0b1-3afc6bd1a23a", BLERead | BLENotify);


BLEService bodyPositionService("931c1539-3ac4-4354-be88-8d77b22d1e3a");
BLEIntCharacteristic bodyPositionChar("305d36fd-cf59-480b-affe-47fa65ac0c0c", BLERead | BLENotify);

/*
****************************************VARIABLES********************************************
*/

/*
Heartbeat sensor vars & funcs
*/
const boolean HAS_A_REF = false; //BUG? analogReference(EXTERNAL) causes a compile error on Arduino 101.
const int PIN_INPUT = A0;
const unsigned long MICROS_PER_READ = 2 * 1000L;
const boolean REPORT_JITTER_AND_HANG = false;
const long OFFSET_MICROS = 1L;  // NOTE: must be non-negative
unsigned long wantMicros;
long minJitterMicros;
long maxJitterMicros;
unsigned long lastReportMicros;
byte samplesUntilReport;
const byte SAMPLES_PER_SERIAL_SAMPLE = 20;
const int PWM_STEPS_PER_FADE = 12;
int fadePWM;
PulseSensorBPM pulseDetector(PIN_INPUT, MICROS_PER_READ / 1000L);
int BPM = 0;
int lastBPM = 0;

void resetJitter() {
  minJitterMicros = 60 * 1000L;
  maxJitterMicros = -1;
}

void updateHeartRate() {
  if (BPM != lastBPM) {
    Serial.print("BPM: "); Serial.println(BPM);
    lastBPM = BPM;
    long sentBPM = BPM;
    heartRateChar.setValue(sentBPM);  // and update the heart rate measurement characteristic
  }
}

/*
Curie vars & funcs
*/

// Stepcount
boolean stepEventsEnabeled = true;
long lastStepCount = 0;



void updateStepCount() {
  int stepCount = CurieIMU.getStepCount();

  if (stepCount != lastStepCount) {
    Serial.print("Step count: "); Serial.println(stepCount);;
    lastStepCount = stepCount;
    stepCountChar.setValue(lastStepCount);
  }
}

/*
Temperature sensor setup & funcs
*/
Adafruit_MLX90614 mlx = Adafruit_MLX90614();
float lastTemperature, currentTemperature;


void updateTemperature() {
  currentTemperature = mlx.readObjectTempC();
  if (currentTemperature != lastTemperature) {
    Serial.print("Temperature: "); Serial.print(currentTemperature); Serial.println("*C");
    lastTemperature = currentTemperature;
    temperatureChar.setValue(currentTemperature);  // and update the heart rate measurement characteristic
    temperatureChar.setValue(currentTemperature);
  }
}






/*
****
Body position
****
*/
float convertRawAceleration(int aRaw) {
  float a = (aRaw * 2.0) / 32768.0;
  return a;
}

void updateBodyPosition() {
  int axRaw, ayRaw, azRaw;
  float ax, ay,az, azNow;



  az = convertRawAceleration(azRaw);

  // delay(100);
  // CurieIMU.readAccelerometer(axRaw, ayRaw, azRaw) ;

  // azNow = convertRawAceleration(azRaw);

  if (azNow-az>=1) {
    // Serial.println("fall");
    bodyPositionChar.setValue(1);
  } else {
    bodyPositionChar.setValue(0);
  }

  // Serial.println(az);


}




/*
****************************************SETUP********************************************
*/

void setup() {
  Serial.begin(9600);



  /*
  Heartbeat sensor init
  */
  if (HAS_A_REF) {}
  samplesUntilReport = SAMPLES_PER_SERIAL_SAMPLE;
  lastReportMicros = 0L;
  resetJitter();
  wantMicros = micros() + MICROS_PER_READ;



  /*
  Curie
  */
  CurieIMU.begin();
  CurieIMU.setStepDetectionMode(CURIE_IMU_STEP_MODE_NORMAL);
  CurieIMU.setStepCountEnabled(true);
  CurieIMU.interrupts(CURIE_IMU_STEP);


  /*
  Body position detection setup
  */
  CurieIMU.setAccelerometerRange(2);

  /*
  Temperature sensor init
  */
  mlx.begin();


  /*
  ****
  Bluetooth Low Energy init
  ****
  */
  blePeripheral.setLocalName("Smart Shi(r)t");

  blePeripheral.setAdvertisedServiceUuid(heartRateService.uuid());  // add the service UUID
  blePeripheral.addAttribute(heartRateService);   // Add the BLE Heart Rate service
  blePeripheral.addAttribute(heartRateChar); // add the Heart Rate Measurement characteristic


  blePeripheral.setAdvertisedServiceUuid(stepCountService.uuid());  // add the service UUID
  blePeripheral.addAttribute(stepCountService);   // Add the BLE Heart Rate service
  blePeripheral.addAttribute(stepCountChar); // add the Heart Rate Measurement characteristic


  blePeripheral.setAdvertisedServiceUuid(temperatureService.uuid());
  blePeripheral.addAttribute(temperatureService);
  blePeripheral.addAttribute(temperatureChar);


  blePeripheral.setAdvertisedServiceUuid(bodyPositionService.uuid());
  blePeripheral.addAttribute(bodyPositionService);
  blePeripheral.addAttribute(bodyPositionChar);



  blePeripheral.begin();
}












/*
****************************************LOOP********************************************
*/
int tempCounter = 0, positionCounter = 0;
int axRaw, ayRaw, azRaw;
float az;

void loop() {
  BLECentral central = blePeripheral.central();

  if (central) {

    while (central) {
      unsigned long nowMicros = micros();
      if ((long) (wantMicros - nowMicros) > 1000L && central) {

        // if (central) {
          updateStepCount();
          updateHeartRate();
          if (++tempCounter == 1000 || tempCounter==0) {
            updateTemperature();
            tempCounter = 1;
          }

          // if (positionCounter == 1) {
          //   CurieIMU.readAccelerometer(axRaw, ayRaw, azRaw) ;
          //   az = convertRawAceleration(azRaw);
          //   Serial.println(az);
          //
          // }
          if (positionCounter==500) {
            CurieIMU.readAccelerometer(axRaw, ayRaw, azRaw) ;
            // Serial.println(convertRawAceleration(azRaw));
            if (az-convertRawAceleration(azRaw)<=-0.7) {
              bodyPositionChar.setValue(1);
              Serial.println(az-convertRawAceleration(azRaw));
            }
            positionCounter = 0;
            az = convertRawAceleration(azRaw);
          }

          positionCounter++;
        // }
        return;
      }

      if ((long) (wantMicros - nowMicros) > 3L + OFFSET_MICROS) {
        delayMicroseconds((unsigned int) (wantMicros - nowMicros) - OFFSET_MICROS);
        nowMicros = micros();
      }

      long jitterMicros = (long) (nowMicros - wantMicros);
      if (minJitterMicros > jitterMicros) {
        minJitterMicros = jitterMicros;
      }
      if (maxJitterMicros < jitterMicros) {
        maxJitterMicros = jitterMicros;
      }

      if (REPORT_JITTER_AND_HANG
          && (long) (nowMicros - lastReportMicros) > 60000000L) {
        lastReportMicros = nowMicros;

        resetJitter();

        for (;;) { }
      }

      wantMicros = nowMicros + MICROS_PER_READ;
      boolean QS = pulseDetector.readSensor();
      if (--samplesUntilReport == (byte) 0) {
        samplesUntilReport = SAMPLES_PER_SERIAL_SAMPLE;
      }


      if (QS) {
        BPM = pulseDetector.getBPM();
      }
    }


  }

  Serial.println("disconnected");
}
